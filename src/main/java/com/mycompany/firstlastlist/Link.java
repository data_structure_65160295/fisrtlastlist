/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.firstlastlist;

/**
 *
 * @author informatics
 */
public class Link {

    public long dData; // data item
    public Link next;

    public Link(long d) // constructor
    {
        dData = d;
    }

    public void displayLink() // display this link
    {
        System.out.print(dData + " ");
    }
}

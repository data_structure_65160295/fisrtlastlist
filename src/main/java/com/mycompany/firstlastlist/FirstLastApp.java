/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.firstlastlist;

/**
 *
 * @author informatics
 */
public class FirstLastApp {

    public static void main(String[] args) { // make a new list
        FirstLastList theList = new FirstLastList();
        theList.insertFirst(22); // insert at front
        theList.insertFirst(44);
        theList.insertFirst(66);
        theList.insertLast(11); // insert at rear
        theList.insertLast(33);
        theList.insertLast(55);
        theList.displayList(); // display the list
        theList.deleteFirst(); // delete first two items
        theList.deleteFirst();
        theList.displayList(); // display again
        theList.deleteLast();
        theList.deleteLast();
        theList.deleteLast();
        theList.deleteLast();
        theList.displayList();
        theList.insertFirst(22);
        theList.displayList();
    }
}
